package de.luh.hci.mi.barcodescanner;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    public static final String FRIDGEPI = "fridgepi";
    public static final String SERVER_IP = "192.168.3.105";

    private final int SERVERPORT = 13337;
    //private static final String SERVER_IP = "fridgepi.local";
    //private static final String SERVER_IP = "192.168.3.116";

    private NetworkServiceDiscovery nsd;
    private String mRPiAddress = "";
    private Thread clientThread;
    private TextView textView;
    private Socket socket;
    private boolean add;
    private PrintWriter out;
    private boolean printWriterError;
    private BufferedReader in;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        textView = (TextView) findViewById(R.id.textView);
        toast = Toast.makeText(getApplicationContext(), getString(R.string.toast_no_connection), Toast.LENGTH_SHORT);

        printWriterError = false;
        connectToRaspberry();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null && scanResult.getContents() != null) {
            String barcode = (add) ? "BARCODE ADD " : "BARCODE REMOVE ";
            barcode += scanResult.getContents();
            textView.setText(barcode);
            if (out != null) {
                out.println(barcode);
                printWriterError = out.checkError();
            }
        } else {
            textView.setText(getString(R.string.scan_failed));
        }
        if (printWriterError || out == null) toast.show();
    }

    public void button_add_pressed(View view) {
        add = true;
        /*
        if (out != null) {
            out.println("BARCODE ADD 4010491003820");
            printWriterError = out.checkError();
        }
        if (printWriterError || out == null) toast.show();
        */
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan(1);
    }

    public void button_remove_pressed(View view) {
        add = false;
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan(1);
    }

    private void connectToRaspberry() {
        clientThread = new Thread(new ClientThread());
        clientThread.start();
    }

    class ClientThread implements Runnable {

        @Override
        public void run() {
            nsd = new NetworkServiceDiscovery(getApplicationContext());
            while (true) {
                if (mRPiAddress.isEmpty()) {
                    mRPiAddress = nsd.getmRPiAddress();
                }
                if (out != null) {
                    out.println("check");
                    printWriterError = out.checkError();
                    Log.d("Fridgepi", printWriterError ? "No Connection" : "Connected");
                }
                if (printWriterError || out == null) {
                    try {
                        InetAddress serverAddr = InetAddress.getByName(mRPiAddress);
                        socket = new Socket(serverAddr, SERVERPORT);
                        out = new PrintWriter(socket.getOutputStream(), false);
                        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    } catch (UnknownHostException e) {
                        Log.d("Fridgepi", "Unknown host: " + mRPiAddress);
                    } catch (IOException e) {
                        Log.d("Fridgepi", "No I/O");
                    }
                }
                try {
                    clientThread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
